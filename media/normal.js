function Normal(o) {
    var defaults = {
        mu: 0.0,
        sigma: 1.0,
        size: '300x100',
        fill_color: '76A4FB', 
        graph_color: '000000', 
        axis_text_color:'000000',
        value_text_color:'AA0088',
        axis_label:'x',
        axis_text:''
    };

    var useleft = true
    var useright = true 
    var usetext = true 

    if(typeof o.right == 'undefined')
        useright = false
    else
        this.right = o.right;

    if(typeof o.left == 'undefined')
        useleft= false
    else
        this.left = o.left;

    if(typeof o.axis_text == 'undefined') 
        usetext = false
    else
        this.axis_text = o.axis_text

    if(typeof o.mu == 'undefined') 
        o.mu = defaults.mu
    if(typeof o.sigma == 'undefined') 
        o.sigma = defaults.sigma
    if(typeof o.size == 'undefined') 
        o.size = defaults.size
    if(typeof o.fill_color == 'undefined') 
        o.fill_color = defaults.fill_color
    if(typeof o.graph_color == 'undefined') 
        o.graph_color = defaults.graph_color
    if(typeof o.axis_text_color == 'undefined') 
        o.axis_text_color = defaults.axis_text_color
    if(typeof o.value_text_color == 'undefined') 
        o.value_text_color = defaults.value_text_color
    if(typeof o.axis_label == 'undefined') 
        o.axis_label = defaults.axis_label

    this.mu = o.mu
    this.sigma = o.sigma

    var r =  this.mu + 4*this.sigma
    var l = this.mu - 4*this.sigma 
    var stepsize = this.sigma/20.0

    var right_index
    if(useright)
        right_index = find_index(r,l,stepsize,this.right)
    else
        right_index = 160

    var left_index
    if(useleft)
        left_index = find_index(r,l,stepsize,this.left)
    else
        left_index = 0 

    var c1 = 1.0/(this.sigma*Math.sqrt(2 * Math.PI))
    var c2 = -1.0/(2*this.sigma*this.sigma)
    //c1 * exp(c2* (x-mu)*(x-mu)) 

    var expr = '' + c1 + '*exp(' + c2 + '*(x-' + this.mu + ')*(x-' + this.mu + '))'   

    this.chartapi = 'http://chart.apis.google.com/chart?'
    this.chs = o.size
    //this.chfd = '0,z,-4.0,4.0,.05,.3989*exp(-.5*z*z)'
    this.chfd = '0,x,' + l + ',' + r + ',' + stepsize + ',' + expr
    this.chds =  '0,' + 1.1*c1
    //this.chds =  '0,' + c1 
    this.chxs = '0,' + o.axis_text_color + ',11,0,l|1,' + o.value_text_color + ',13,0,t|2,000000,11,0,_'
    //this.chxr = '0,-4.0,4.0|1,-4.0,4.0'
    this.chxr = '0,'+ l + ',' + r + '|1,' + l + ',' + r 
    this.chxtc = '1,5'
    this.cht = 'lc'
    this.chd = 't:-1'
    this.chco = o.graph_color
    this.chxt = 'x,x,y'
    this.chm='B,' + o.fill_color +',0,' + left_index + ':' + right_index + ',0'
    this.chxl =''
    this.chxp =''

    // Next we do the labeling on the axes. There are 3 of them: x,x,y
    // The first two "x"s are for the x-axis label and z-value(s)
    // The y one is just there to make it invisible
    this.chxl = '0:|' + this.mu + '|' + o.axis_label + '-axis'   
    this.chxp = '0,' + this.mu + ',' + (this.mu+5*this.sigma) 

    if(useright && useleft) {
        if(usetext)
            this.chxl += '|' + '1:|' + this.axis_text + '|' + this.axis_text + '|' + o.axis_label + ' values'
        else
            this.chxl += '|' + '1:|' + this.left + '|' + this.right + '|' + o.axis_label + ' values'
        this.chxp += '|' + '1,' + this.left + ',' + this.right + ',' +  (this.mu+5*this.sigma)
    }
    else if (useright) {
        if(usetext)
            this.chxl +='|' + '1:|' + this.axis_text + '|' + o.axis_label + ' value'
        else
            this.chxl +='|' + '1:|' + this.right + '|' + o.axis_label + ' value'
        this.chxp +='|' + '1,' + this.right +',' + (this.mu+5*this.sigma)
    }
    else if (useleft) {
        if(usetext)
            this.chxl +='|' + '1:|' + this.axis_text + '|' + o.axis_label + ' value'
        else
            this.chxl +='|' + '1:|' + this.left + '|' + o.axis_label + ' value'
        this.chxp +='|' + '1,' + this.left + ',' + (this.mu+5*this.sigma)

    }
    else {
        this.chxl +='|' + '1:||'
    }

    this.chxl += '|'+ '2:||'
}

Normal.prototype.getChartURL = function() {
    return this.chartapi + 
          'cht=' + this.cht + '&' + 
          'chd=' + this.chd + '&' +
          'chs=' + this.chs + '&' +
          'chco=' + this.chco + '&' +
          'chfd=' + this.chfd + '&' +
          'chxt=' + this.chxt + '&' +
          'chxs=' + this.chxs + '&' +
          'chds=' + this.chds + '&' +
          'chxl=' + this.chxl + '&' +
          'chxp=' + this.chxp + '&' +
          'chxr=' + this.chxr + '&' +
          'chxtc=' + this.chxtc + '&' +
          "chm=" + this.chm;
}

function find_index(upper,lower,delta,value) {
   var total = Math.round((upper-lower)/delta)
   var index

   //console.log("total is " + total);

    if(value > upper) 
        index = total 
    else if(value < lower)
        index = 0
    else
        index = Math.round((value-lower)/delta)

   //console.log("index is " + index);
   return index
}

// left tail area = .7500 
function latex_left_tail_backwards_area(area) {
   var latex = '\\text{left tail area}'
   latex += '=' + round(area,4)
   return latex
}

//z=1.25 
function latex_left_tail_backwards_zvalue(area) {
  var latex = 'z=' + round(normsinv(area),2) 
  return latex
}

//left tail area = 1.0 - right tail area = 1.0 - .75 = .25 
function latex_one_minus_right_tail_area(area) {
  var latex = '\\text{left tail area}'
  latex += '= 1.0 - \\text{right tail area}'
  latex += '= 1.0 -' + area
  latex += '=' + round(1.0-area,4)
  return latex
}

//left tail area = .3424
function latex_left_tail_area(z) {
   var latex = '\\text{left tail area}'
   latex += '=' + round(stdnormalcdf(z),4)
   return latex
}

//right tail area = 1.0 - left tail area = 1.0-blah = answer
function latex_right_tail_area(z) {
   var latex = '\\text{right tail area}'
   latex += '=1.0-\\text{left tail area}'
   latex += '=1.0 - ' + round(stdnormalcdf(z),4)
   latex += '=' + round(1.0 - stdnormalcdf(z),4)
   return latex
}

//area between = left tail upper - left tail lower = .3423 - .1935 
function latex_area_between(b,a) {
   var latex = '\\text{area between}' 
   latex += '= \\text{left tail upper} - \\text{left tail lower}'
   latex += '=' + round(stdnormalcdf(b),4) + '-' + round(stdnormalcdf(a),4)
   latex += '=' + round(stdnormalcdf(b) - stdnormalcdf(a),4)
   return latex
}

function latex_left_tail_backwards_xvalue(area, mu, sigma) {
   var z = normsinv(area)
   var x = round(z,2)*sigma + mu
   var latex = 'z=\\frac{x - \\mu}{\\sigma}' 
   latex += '\\\\'
   latex += round(z,2) + '=\\frac{x-' + mu + '}{' + sigma + '}'
   latex += '\\\\'
   latex += round(z,2) + '(' + sigma + ')=x-' + mu
   latex += '\\\\'
   latex += round(z,2) + '(' + sigma + ')+' + mu + '=x'
   latex += '\\\\'
   latex += round(x,2) + '=x'
   return latex
}

function latex_zvalue_means(xbar,mu,sigma,n) {
   var latex = 'z'
   latex += '=\\frac{\\bar x - \\mu}{\\frac{\\sigma}{\\sqrt{n}}}'
   latex += '=\\frac{' + xbar + '-' + mu + '}{\\frac{'+sigma+'}{\\sqrt{'+n+'}}}'
   latex += '=\\frac{' + round(xbar - mu,2) + '}{' + round(sigma/Math.sqrt(n),2) + '}'
   latex += '=' + round((xbar - mu)/(sigma/Math.sqrt(n)), 2)
   return latex
}

function latex_zvalue(x,mu,sigma) {
   var latex = 'z'
   latex += '=\\frac{x - \\mu}{\\sigma}'
   latex += '=\\frac{' + x + '-' + mu + '}{' + sigma + '}'
   latex += '=\\frac{' + round(x - mu,2) + '}{' + sigma + '}'
   latex += '=' + round((x - mu)/sigma, 2)
   return latex
}

function stdnormalcdf(Z) {   
    //HASTINGS.  MAX ERROR = .000001
    var Prob = 0;
    var T=1/(1+.2316419*Math.abs(Z));
    var D=.3989423*Math.exp(-Z*Z/2);
    Prob=D*T*(.3193815+T*(-.3565638+T*(1.781478+T*(-1.821256+T*1.330274))));
    if (Z>0) 
            Prob=1-Prob;

    return Prob;
}   

function normsinv(p) {
  //
  // Lower tail quantile for standard normal distribution function.
  //
  // This function returns an approximation of the inverse cumulative
  // standard normal distribution function.  I.e., given P, it returns
  // an approximation to the X satisfying P = Pr{Z <= X} where Z is a
  // random variable from the standard normal distribution.
  //
  // The algorithm uses a minimax approximation by rational functions
  // and the result has a relative error whose absolute value is less
  // than 1.15e-9.
  //
  // Author:      Peter J. Acklam
  // (Javascript version by Alankar Misra @ Digital Sutras (alankar@digitalsutras.com))
  // Time-stamp:  2008-04-15 10:32:14 +02:00
  // E-mail:      pjacklam@online.no
  // WWW URL:     http://home.online.no/~pjacklam

  // An algorithm with a relative error less than 1.15*10-9 in the entire region.

  // Coefficients in rational approximations
  var a = new Array(-3.969683028665376e+01,  2.209460984245205e+02,
                    -2.759285104469687e+02,  1.383577518672690e+02,
                    -3.066479806614716e+01,  2.506628277459239e+00);
  var b = new Array(-5.447609879822406e+01,  1.615858368580409e+02,
                    -1.556989798598866e+02,  6.680131188771972e+01,
                    -1.328068155288572e+01 );
  var c = new Array(-7.784894002430293e-03, -3.223964580411365e-01,
                    -2.400758277161838e+00, -2.549732539343734e+00,
                     4.374664141464968e+00,  2.938163982698783e+00);
  var d = new Array (7.784695709041462e-03,  3.224671290700398e-01,
                     2.445134137142996e+00,  3.754408661907416e+00);

  // Define break-points.
  var plow  = 0.02425;
  var phigh = 1 - plow;

  // Rational approximation for lower region:
  if ( p < plow ) {
           var q  = Math.sqrt(-2*Math.log(p));
           return (((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) /
                                           ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1);
  }

  // Rational approximation for upper region:
  if ( phigh < p ) {
           var q  = Math.sqrt(-2*Math.log(1-p));
           return -(((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) /
                                                  ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1);
  }

  // Rational approximation for central region:
  var q = p - 0.5;
  var r = q*q;
  return (((((a[0]*r+a[1])*r+a[2])*r+a[3])*r+a[4])*r+a[5])*q /
                           (((((b[0]*r+b[1])*r+b[2])*r+b[3])*r+b[4])*r+1);
}
