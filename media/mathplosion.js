var mathplosion_editquestion = false;
var mathplosion_editanswer = false;

$(document).ready(function() {
   if (typeof json_params !== 'undefined') {
        setup_params();
   }
}); 

function setup_params() {

    if($("#form").length === 0) {
        $('body').prepend('<form id="form"></form>'); 
    }

    for(param in json_params) {

       //var lab = '<span> ' + param +' </span>';
       //$("#form").AppendTo(lab);
       //
       //Build the html input element for the param.  
       //var input = lab + '<input class="input-small" type="text"' + 
       //             ' id=' + id_str +
       //             ' name=' + name_str +    
       //           '/>';
       //
       var param_quoted = '"' + param + '"';
       var id_quoted = '"id_' + param + '"';
       var input = '<label for=' + id_quoted + '>' + param + 
                   ' <input class="input-mini" type="text"' + 
                    ' id=' + id_quoted +
                    ' name=' + param_quoted +  '></label>';

       //Put the input element into the params 
       if($("#formparams").length === 1) {
            $(input).appendTo("#formparams");
       }
       else {
            $(input).appendTo("#form");
       }

       // Set the initial value for the text box to the default value.
       $('#id_' + param).val(json_params[param].val);

       // Set update to be called when the input text box changes.
       $('#id_' + param).keyup(update);
       $('input[name=' + param + ']').change(update)
    }
    update();
}

function update() {

   var params = {};

   for(key in json_params) {
       var newval=$('#id_' + key).val()
       params[key]=newval;
       json_params[key].val=newval;
   };

   update_params(params);
}

function get_selector(selector) {
   var q = mathplosion_editquestion;
   var a = mathplosion_editanswer;
   var question, answer, join;

   if (q === false && a === false){
      join = $(selector);
   }
   else if (q === true && a === false){
      question = $("#question_textarea_ifr").contents().find(selector);
      answer = $("#answer " + selector); 
      join = question.add(answer)
   }
   else if (q === false && a === true){
      answer = $("#answer_textarea_ifr").contents().find(selector);
      question = $("#question " + selector); 
      join = question.add(answer)
   }
   if (q === true && a === true){
      question = $("#question_textarea_ifr").contents().find(selector);
      answer = $("#answer_textarea_ifr").contents().find(selector);
      join = question.add(answer)
   }

   return join 
}

function update_spans(class_name, value) {
   var selector = "." + class_name;
   var $selector = get_selector(selector);
   $selector.text(value);
}

function update_class_equations(class_name, latex) {
   var selector = "." + class_name
   update_equation(selector, latex)
}

function update_id_equation(id, latex) {
   var selector = "#" + id 
   update_equation(selector, latex)
}

function update_equation(selector, latex) {

   var $selector = get_selector(selector);

   var encodedLatex = encodeURIComponent(latex);

   var url = "http://chart.apis.google.com/chart?cht=tx&chs=1x0&chf=bg,s,FFFFFF00&chco=000000&chl=" + encodedLatex;

//   var debug_url = "http://chart.apis.google.com/chart?cht=tx&chs=1x0&chf=bg,s,FFFFFF00&chco=000000&chl=" + decodeURIComponent(encodedLatex);

//    console.log("$selector is" + $selector);
//    console.log("debug_url is " + debug_url);
//    console.log("latex is" + latex);

     $selector.attr({
                  src: url,
                  "data-mce-src": url,  //hack for tinymce editor
                  alt: latex 
              });
   $selector.addClass("mathpl")
}

function update_id_graph(id, graph) {
   var selector = "#" + id 
   update_graph(selector, graph)
}

function update_graph(selector, graph) {
   //should encode the URL here...
   var $selector = get_selector(selector);
   $selector.attr('src', graph.getChartURL()) 
   $selector.attr('data-mce-src', graph.getChartURL()) 
   $selector.addClass("mathpl")
}

function update_show_div_id(id) {
   var selector = "#" + id 
   var $selector = get_selector(selector);
   $selector.show()
}

function update_hide_div_id(id) {
   var selector = "#" + id 
   var $selector = get_selector(selector);
   $selector.hide()
}

function round(x,n) {
  return Math.round(x*Math.pow(10,n))/Math.pow(10,n);
}

