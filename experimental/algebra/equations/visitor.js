/* visitor module for algebraic equations */

var Visitor = function() {
};
methods(Visitor, {
    visit_terminal: function (node) {},
    visit_nonterminal: function (op) {},
    visit_literal: function (literal) {
        this.visit_terminal(literal);
    },
    visit_name: function (name) {
        this.visit_terminal(name);
    },
    visit_unaryminus: function (op) {
        this.visit_nonterminal(op);
    },
    visit_unaryplus: function (op) {
        this.visit_nonterminal(op);
    },
    visit_div: function (op) {
        this.visit_nonterminal(op);
    },
    visit_minus: function (op) {
        this.visit_nonterminal(op);
    },
    visit_power: function (op) {
        this.visit_nonterminal(op);
    },
    visit_plus: function (op) {
        this.visit_nonterminal(op);
    },
    visit_mult: function (op) {
        this.visit_nonterminal(op);
    }
});

var Dfs = function() {
};
extend(Dfs, Visitor);
methods(Dfs, {
    traverse: function (node) { 
        if(!node.is_terminal) {
            for(var i=0; i < node.operands.length; i++) {
                this.traverse(node.operands[i]);
            }
        }
        node.accept_visit(this);
    }
});

var Copy = function() {
    Dfs.call(this);
    this.stack = [];
};
extend(Copy, Dfs);
methods(Copy, {
    visit_terminal: function (node) {
        var n = new (node.klass)(node.value);
        this.stack.push(n); 
    },
    visit_nonterminal: function(op) {
        var l = op.operands.length;
        var operands = this.stack.splice(-l, l);
        var op = new (op.klass)(operands);
        this.stack.push(op);
    }
});

var Replace = function(node,new_node) {
    Copy.call(this);
    this.node = node;
    this.new_node = new_node;
};
extend(Replace, Copy);
methods(Replace, {
    visit_terminal: function (node) {
        if(this.node === node) {
            this.stack.push(this.new_node); 
        }
        else {
            Replace.superclass.visit_terminal.call(this,node);
        }
    },
    visit_nonterminal: function(op) {
        if(this.node === op) {
            //pop the old operands off the stack, we won't need them. 
            var l = op.operands.length;
            var operands = this.stack.splice(-l, l);

            //push the new node on the stack. 
            this.stack.push(this.new_node); 
        }
        else {
            Replace.superclass.visit_nonterminal.call(this,op);
        }
    }
});

var Simplify = function()  {
    Replace.call(this);
    this.simplified = false;
};
extend(Simplify, Replace);
methods(Simplify, {
    simplify: function (simplifier) {
        if(this.simplified == false 
           && simplifier.check_simplify_evaluate() === true) {
            this.node = simplifier.op;
            this.new_node = simplifier.simplify_evaluate(); 
            this.simplified = true;
            return true;
        }
        return false;
    }, 
    visit_unaryminus: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_unaryminus.call(this,op);
    },
    visit_unaryplus: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_unaryplus.call(this,op);
    },
    visit_div: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_div.call(this,op);
    },
    visit_minus: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_minus.call(this,op);
    },
    visit_power: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_power.call(this,op);
    },
    visit_plus: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_plus.call(this,op);
    },
    visit_mult: function (op) {
        this.simplify(new SimplifyBase(op));
        Simplify.superclass.visit_mult.call(this,op);
    }
});

var Steps = function() {
    Simplify.call(this);
    this.asts = [];
};
extend(Steps, Simplify);
methods(Steps, {
    step: function(ast) {
        this.traverse(ast); 
        return this.stack.pop(); 
    },
    check_done: function (next,prev) {
        if(infix(next) === infix(prev))
            return true;
        return false;
    },
    simplify_steps: function (root) {
        this.asts.push(root);

        var prev = root;
        var next = null;

        var done = false;
        while (done !== true) {
           this.simplified = false;
           next = this.step(prev);
           if(this.check_done(next,prev)) {
              done = true;
           }
           else {
              this.asts.push(next);
              prev = next;
           }
        }
    }
});

var Flatten = function() {
    Copy.call(this);
};
extend(Flatten, Copy);
methods(Flatten, {
    visit_nary: function (op) {
        var l = op.operands.length;

        //Pop the old operands off the stack.
        var operands = this.stack.splice(-l, l);

        var new_operands = []; 
        for(var i=0; i < l; i++) {
            if (operands[i] instanceof op.klass) {
               new_operands = new_operands.concat(operands[i].operands);
            }
            else {
               new_operands.push(operands[i]);
            }
        }

        var new_op = new op.klass(new_operands);
        this.stack.push(new_op);
    },
    visit_plus: function (op) {
        this.visit_nary(op);
    },
    visit_mult: function (op) {
        this.visit_nary(op);
    }
});

var Infix = function() {
    Dfs.call(this);
    this.stack = [];
};
extend(Infix, Dfs);
methods(Infix, {
    visit_terminal: function (node) {
        var str = node.value.toString();
        this.stack.push(str); 
    },
    visit_nonterminal: function(op) {
        var l = op.operands.length;
        var operands = this.stack.splice(-l, l);
        for(var i=0; i < l; i++) {
            var operand = op.operands[i];
            //If precedence of operand is lower than op, put ()
            if (!operand.is_terminal &&  
                (operand.precedence < op.precedence ||
                 op instanceof Power)) {
                operands[i] = "(" + operands[i] + ")";
            }
        }
        var str = operands.join(op.value);
        this.stack.push(str);
    }
});

var Sexpr = function() {
    Dfs.call(this);
    this.stack = [];
};
extend(Sexpr, Dfs);
methods(Sexpr, {
    visit_terminal: function (node) {
        var str = node.value.toString();
        this.stack.push(str); 
    },
    visit_nonterminal: function(op) {
        var l = op.operands.length;
        var operands = this.stack.splice(-l, l);
        var str = "(";
        str += op.value + " ";
        str += operands.join(" ");
        str += ")";
        this.stack.push(str);
    }
});

var Eval = function() {
    Dfs.call(this);
    this.stack = [];
};
extend(Eval, Dfs);
methods(Eval, {
    visit_terminal: function (node) {
        var value = node.evaluate();
        this.stack.push(value); 
    },
    visit_nonterminal: function(op) {
        var l = op.operands.length;
        var operands = this.stack.splice(-l, l);
        var value = op.evaluate(operands);
        this.stack.push(value);
    }
});

function replace (ast, node, new_node) {
    var v = new Replace(node, new_node);
    v.traverse(ast);
    return v.stack.pop();
}

function copy (ast) {
    var v = new Copy();
    v.traverse(ast);
    return v.stack.pop();
}

function flatten (ast) {
    var v = new Flatten();
    v.traverse(ast);
    return v.stack.pop();
}

function sexpr (ast) {
    var v = new Sexpr();
    v.traverse(ast);
    return v.stack.pop();
}

function infix (ast) {
    var v = new Infix();
    v.traverse(ast);
    return v.stack.pop();
}

function evaluate (ast) {
    var v = new Eval();
    v.traverse(ast);
    return v.stack.pop();
}

function steps(ast) {
    var s = new Steps(); 
    s.simplify_steps(ast);
    return s.asts;
} 

function step(ast) {
    var v = new Simplify(); 
    v.traverse(ast);
    return v.stack.pop();
} 

function get_json (ast) {
    return JSON.stringify(ast, 
      ['klass','klass_name','value', 'operands'], 2);
}

function parse_and_flatten(infix) {
    var unsimp = algebra.parse(infix);
    return flatten(unsimp);
} 
