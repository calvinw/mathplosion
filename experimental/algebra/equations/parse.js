/* (C)2009 Kevin Mehall (http://kevinmehall.net)
 * Licensed under the terms of the GNU GPLv2 or greater
 * This file is part of EquationExplorer (http://labs.kevinmehall.net/equationexplorer/
 */

/* TDOP parser for algebraic equations */

var parse = function () {

      //private variables
      var ast = null;
      var symbol_table = {};
      var token;
      var tokens;
      var token_nr;

      var original_symbol = {
          nud: function () {
              error("Undefined.", this);
          },
          led: function (left) {
              error("Missing operator.", this);
          },
      };

      var symbol = function (id, bp) {
          var s = symbol_table[id];
          bp = bp || 0;
          if (s) {
              if (bp >= s.lbp) {
                  s.lbp = bp;
              }
          } else {
              s = Object.create(original_symbol);
              s.id = s.value = id;
              s.lbp = bp;
              symbol_table[id] = s;
          }
          return s;
      };


      var advance = function (id) {
          var a, o, t, v;
          if (id && token.id !== id) {
              error("Expected '" + id + "'.", token);
          }
          if (token_nr >= tokens.length) {
              token = symbol_table["(end)"];
              return;
          }
          t = tokens[token_nr];
          token_nr += 1;
          v = t.value;
          a = t.type;
          if (a === "name") {
              o = symbol_table["(name)"];
          } else if (a === "operator") {
              o = symbol_table[v];
              if (!o) {
                  error("Unknown operator.", t);
              }
          } else if (a ===  "number") {
              a = "literal";
              o = symbol_table["(literal)"];
          } else {
              error("Unexpected token.", t);
          }

          token = Object.create(o);
          token.value = v;
          token.arity = a;
          return token;
      };

      var expression = function (rbp) {
          var left, t;
          t = token;
          advance();
          left = t.nud();

          while (rbp < token.lbp) {
              t = token;
              advance();
              left = t.led(left);
          }
          return left;
      }

      symbol("(end)");
      symbol("(name)", 58).nud = function() {
          //return ast_module.create_name(this.value);
          return new Name(this.value);
      };

      symbol("(literal)", 60).nud = function() {
          //return ast_module.create_literal(this.value);
          return new Literal(this.value);
      };

      var infix_nary = function (id, bp, led) {
          var s = symbol(id, bp);
          s.led = led || function (left) {

              var operands = [];
              if (left.value === this.value) { 
                  operands = left.operands.slice(0)
                  operands.push(expression(bp));
              }
              else {
                  operands[0] = left;
                  operands[1] = expression(bp);
              }

              //return ast_module.create_nary(this.value, operands);
              return new Nary(this.value, operands);
          };
          return s;
      }

      var infix = function (id, bp, led) {
          var s = symbol(id, bp);
          s.led = led || function (left) {
              var operands = [];
              operands[0] = left;
              operands[1] = expression(bp);
              //return ast_module.create_binary(this.value, operands);
              return new Binary(this.value, operands);
          };
          return s;
      }

      infix_nary("+", 50);
      infix("-", 50);
      infix_nary("*", 60);
      infix("/", 60);
      infix("^", 70);
      
      symbol("(").nud = function () {
          var e = expression(0);
          advance(")");
          return e;
      }
      
      symbol("(", 61).led = function(left){
          var e = expression(0);
          advance(")");
/*
          return {
                    value:'*', 
                    arity:'nary', 
                    operands: [left, e]
                  }
*/
          return new Nary("*", [left, e]); 
      }
      
      symbol(")");
      symbol("-").nud = function () {
/*
          return {
                      value:'-', 
                      arity:'binary', 
                      operands: [{value:0, arity:'literal'}, expression(65)]
                 }
*/

          return new Binary("-", [new Literal(0), expression(65)]); 
      }

    return {

      //public methods 
      get_ast: function() {
          return ast;
      },

      get_tokens: function() {
          return tokens;
      },

      get_symbol_table: function() {
          return symbol_table;
      },

      make_ast: function (source) {
          //String was patched with tokens method..!
          tokens = source.tokens('/+-*^()');
          token_nr = 0;
          advance();
          ast = expression(0);
          advance("(end)");
          return ast;
      }
    };
}

parse_module=parse();
