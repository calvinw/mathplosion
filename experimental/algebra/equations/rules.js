
var SimplifyBase = function (op) {
    this.op = op;
};
methods(SimplifyBase, {
    check_simplify_evaluate : function () {
        var operands = this.op.operands;
        for(var i=0; i < operands.length; i++) {
            if(operands[i] instanceof Literal) {
                continue; 
            }
            else {
                return false;
            }
        }
        return true;
    },
    simplify_evaluate : function () {
        var operands = this.op.operands;
        var values = [];
        for(var i=0; i < operands.length; i++) {
            values[i] = operands[i].value; 
        }
        var value = this.op.evaluate(values);
        return new Literal(value);
    }
});

/*
var FlattenRule = function (op) {
    Rule.call(this, op);
};
extend(FlattenRule, Rule) 
methods(FlattenRule,{
    check : function () {
        var ops = this.op.operands;
        for(var i=0; i < ops.length; i++) {
            if (ops[i] instanceof this.op.klass) {
                return true;
            }
        }
        return false;
    },
    simplify : function () {
        var ops = this.op.operands;
        var new_ops = []; 
        for(var i=0; i < ops.length; i++) {
            if (ops[i] instanceof this.op.klass) {
               new_ops = new_ops.concat(ops[i].operands);
            }
            else {
               new_ops.push(ops[i]);
            }
        }
        return new this.op.klass(new_ops);
    }
});
*/

/* (x^2)^3 = x^(2*3) */
var DoublePower = {
    name: "doublepower",
    check : function (op) {
        if(op.operands[0] instanceof Power && 
           op.operands[0].operands[0] instanceof Name &&
           op.operands[0].operands[1] instanceof Literal &&
           op.operands[1] instanceof Literal) {
           return true;
        }
        return false;
    },
    simplify : function (op) {
        var b = op.operands[0].operands[0];
        var e1 = op.operands[0].operands[1];
        var e2 = op.operands[1];
        var exp = new Mult([e1,e2]);
        return new Power([b,exp]); 
    }
};

/* (x*y)^3 = x^3*y^3 */
var DistributePower = {
    name: "distributepower",
    check : function (op) {
        if(op instanceof Power &&
           op.operands[0] instanceof Mult &&
           op.operands[1] instanceof Literal) {
           return true;
        }
        
        return false;
    },
    simplify : function (op) {
        var mult = op.operands[0];
        var exp = op.operands[1];
        var factors = [];
        for(var i=0; i < mult.operands.length; i++) {
            var b = mult.operands[i];
            var e = new Literal(exp.value);
            factors.push(new Power([b,e]))
        }
        return new Mult(factors); 
    }
};

/* x^4*x^3 = x^(3+4) */
var CombineFactors = {
    name: "combinefactors",
    check : function (op) {
        if(!op instanceof Mult) {
            return false;
        }
        var i;
        var operands = op.operands;
        for(i=0; i < operands.length; i++) {
            if(operands[i] instanceof Power){
                continue; 
            }
            else {
                return false;
            }
        }
        return true;
    },
    simplify : function (op) {
        var operands = op.operands;
        var b = operands[0].operands[0];
        var e1 = operands[0].operands[1];
        var e2 = operands[1].operands[1];
        var e = new Plus([e1,e2]);
        return new Power([b,e]); 
    }
};

/* x*y*x = x*x*y */
var ArrangeFactors = {
    name: "arrangefactors",
    factors: {},
    check : function (op) {
        if(!op instanceof Mult) {
            return false;
        }
        this.factors = {};
        for(var i=0; i < op.operands.length; i++) {
            var o = op.operands[i]
            if(o instanceof Name) {
                var name = o.value;
                if (!(name in this.factors)) 
                    this.factors[name]=[];
                this.factors[name].push(o)
           }
        }
        return true;
    },
    simplify : function (op) {
        var new_operands = []; 
        for (var key in this.factors) {
            new_operands = new_operands.concat(this.factors[key]);
        }
        return new Mult(new_operands);
    }
};

var Evaluate = {
    name: "evaluate",
    check : function (op) {
        var i;
        var operands = op.operands;
        for(i=0; i < operands.length; i++) {
            if(operands[i] instanceof Literal) {
                continue; 
            }
            else {
                return false;
            }
        }
        return true;
    },
    simplify : function (op) {
        var operands = op.operands;
        var values = [];
        var i;
        for(i=0; i < operands.length; i++) {
            values[i] = operands[i].value; 
        }
        var value = op.evaluate(values);
        return new Literal(value);
    }
};
