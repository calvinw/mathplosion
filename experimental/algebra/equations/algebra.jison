/* lexical grammar */
%lex
%%
[ \t\r\v\f]+          /* nothing */
[ \n]+                /*return 'EOL'*/
[0-9]+("."[0-9]+)?\b  return 'NUMBER'
[a-z]                 return 'NAME'
"*"                   return '*'
"/"                   return '/'
"-"                   return '-'
"^"                   return '^'
"+"                   return '+'
"("                   return '('
")"                   return ')'

/lex

%start list 

%% /* algebraic expressions, one per line */

list : Exp                 {$$=$1; return $$;}
     ;

Exp    : Exp '+' Term      {$$ = new Plus([$1,$3]);}
       | Exp '-' Term      {$$ = new Minus([$1,$3]);}
       | Term
       ;

Term   : Term '*' Factor   {$$ = new Mult([$1,$3]);}
       | Term '/' Factor   {$$ = new Div([$1,$3]);}
       | Factor
       ;

Factor : Primary
       | Primary '^' Factor {$$ = new Power([$1,$3]);}
       ;

Primary: NegPrimary
       | PosPrimary
       | Element
       ;

NegPrimary: '-' Primary     {$$ = new UnaryMinus([$2]);}
          ;

PosPrimary: '+' Primary     {$$ = new UnaryPlus([$2]);}
          ;

Element : Terminal
        | '(' Exp ')'       {$$=$2;}
        ;

Terminal: NUMBER            {$$ = new Literal(parseFloat($1));}
        | NAME              {$$ = new Name($1);}
        ;
