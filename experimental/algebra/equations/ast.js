
var Expression = function () {
};

var Terminal = function () {
};
extend(Terminal, Expression);
methods(Terminal, {
    is_terminal: true 
});

var NonTerminal = function (operands) {
    this.operands = operands;
};
extend(NonTerminal, Expression);
methods(NonTerminal, {
    is_terminal: false,
    literals_front: function () {
        var num = this.literals().length;
        for(var i = 0; i < num; i++) {
            if(this.operands[i] instanceof Literal) {
                continue;
            }
            else {
                return false;
            }
        }
        return true;
    },
    literals: function () {
        var literals = [];
        for(var i = 0; i < this.operands.length; i++) {
            if(this.operands[i] instanceof Literal)
                literals.push(this.operands[i]);
        }
        return literals;
    }
});

var Literal = function (value) {
    this.value = value;
};
extend(Literal, Terminal);
methods(Literal, {
    klass: Literal,
    klass_name: "literal",
    accept_visit: function(v) {
        return v.visit_literal(this); 
    },
    evaluate: function (values) {
        return this.value;
    }
});

var Name = function(value) {
    this.value = value;
};
extend(Name, Terminal);
methods(Name, {
    klass: Name,
    klass_name: "name",
    accept_visit: function(v) {
        return v.visit_name(this); 
    },
    evaluate: function (values) {
        return this.value;
    }
});

var UnaryMinus = function (operands) {
    NonTerminal.call(this, operands);
};
extend(UnaryMinus, NonTerminal);
methods(UnaryMinus, {
    klass: UnaryMinus,
    klass_name: "unaryminus",
    value: "-",
    precedence: 3,
    accept_visit: function(v) {
        return v.visit_unaryminus(this); 
    },
    evaluate: function (values) {
        return -values[0]; 
    }
});

var UnaryPlus = function (operands) {
    NonTerminal.call(this, operands);
};
extend(UnaryPlus, NonTerminal);
methods(UnaryPlus, {
    klass: UnaryPlus,
    klass_name: "unaryplus",
    value: "+",
    precedence: 3,
    accept_visit: function(v) {
        return v.visit_unaryplus(this); 
    },
    evaluate: function (values) {
        return values[0]; 
    }
});

var Plus = function (operands) {
    NonTerminal.call(this, operands);
};
extend(Plus, NonTerminal);
methods(Plus, {
    klass: Plus,
    klass_name: "plus",
    value: "+",
    precedence: 1,
    accept_visit: function(v) {
        return v.visit_plus(this); 
    },
    evaluate: function (values) {
        var result=0;
        for(var i=0; i < values.length; i++) {
            result = result + values[i];
        }
        return result;
    }
});

var Mult = function (operands) {
    NonTerminal.call(this, operands);
};
extend(Mult, NonTerminal);
methods(Mult, {
    klass: Mult,
    klass_name: "mult",
    value: "*",
    precedence: 2,
    accept_visit: function(v) {
        return v.visit_mult(this); 
    },
    evaluate: function (values) {
        var result=1;
        for(var i=0; i < values.length; i++) {
            result = result * values[i];
        }
        return result;
    }
});

var Minus = function (operands) {
    NonTerminal.call(this, operands);
};
extend(Minus, NonTerminal);
methods(Minus, {
    klass: Minus,
    klass_name: "minus",
    value: "-",
    precedence: 1,
    accept_visit: function(v) {
        return v.visit_minus(this); 
    },
    evaluate: function (values) {
       return values[0]-values[1]; 
    }
});

var Div = function (operands) {
    NonTerminal.call(this, operands);
};
extend(Div, NonTerminal);
methods(Div, {
    klass: Div,
    klass_name: "div",
    value: "/",
    precedence: 2,
    accept_visit: function(v) {
        return v.visit_div(this); 
    },
    evaluate: function (values) {
       return values[0]/values[1]; 
    }
});

var Power = function (operands) {
    NonTerminal.call(this, operands);
};
extend(Power, NonTerminal);
methods(Power, {
    klass: Power,
    klass_name: "power",
    value: "^",
    precedence: 4,
    accept_visit: function(v) {
        return v.visit_power(this); 
    },
    evaluate: function (values) {
       return Math.pow(values[0],values[1]); 
    }
});

