export DJANGO_SETTINGS_MODULE=settings

# Make sure mathplosion, django-registration, 
# django-profiles, and django-taggit are all in 
# the PYTHONPATH 

export MATHPLOSION=`pwd`
export PYTHONPATH=$MATHPLOSION
