#!/usr/bin/env python
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from editor.models import Template, MpTag 
from editor.utils import extract_template_html
from taggit.forms import *
import glob
import sys 
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'mathplosion.settings'

def extract(text, sub1, sub2):
    """extract a substring between two substrings sub1 and sub2 in text"""
    return text.split(sub1)[-1].split(sub2)[0]

from django.conf import settings
def readtemplate(filename):

    print "opening file %s" % filename 
    fullname = settings.ROOT_PATH + "/editor/fixtures/" + filename 
    f = open(fullname,"r")
    html = f.read()

    user = User.objects.get(pk=1)

    template = Template(user=user)
    template = extract_template_html(template, html)

    name = extract(html, '{% name "','" %}')
    tags = extract(html, '{% tags "','" %}')

    template.filename=filename
    template.name=name
    template.save()

    user = User.objects.get(username = "mathplosion")

    taglist = tags.split(",")
    template.replace_tags(taglist)
    template.save()

    print "Done saving template"
    return

#readtemplate("sumoftwonumbers.html");
#readtemplate("multiplybytwo.html");
#readtemplate("areabetweenmeans.html");

path = settings.ROOT_PATH + "/editor/fixtures"
for file in glob.glob(os.path.join(path, '*.html')):
    base = os.path.basename(file)
    readtemplate(base)
