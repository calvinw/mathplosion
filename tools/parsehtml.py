#!/usr/bin/env python
 
import unittest 
import HTMLParser

html="""<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://127.0.0.1:8000/media/mathplosion.js"></script>

<!--
{% name "Multiply number by two" %}
{% tags "demo" %}
-->

<script id="params">
var json_params =    
{
    "z":{"type":"int","val":"3"}
}
</script>

<script class="library" src="http://127.0.0.1:8000/media/normal.js"></script>
<script id="javascript">
function update_params(params) {
    var zstring = params.z;
    var z = parseInt(zstring);
    var result = 2 * z;
    var resultstring = parseFloat(result);

    update_spans("z", z)
    update_class_equations("z_equals", "z=" + z)  
    var latex = "2z = 2(z) = 2(" + zstring + ") = " + resultstring
    update_id_equation("twotimesz",latex)
}
</script>

<h2>Question:</h2>
<div id="question">
<p> Find the result of 2 times <span class="z"></span>.</p>
<div> <p>inside div </p> </div>
</div>

<h2>Answer:</h2>
<div id="answer">

<p>We have <img class="z_equals eqn"/> and so:</p>

<!-- this is my next thing -->
<img id="twotimesz" class="eqn"/>

</div>"""

class MathplosionParser(HTMLParser.HTMLParser):

    def __init__(self):

        HTMLParser.HTMLParser.__init__(self)
        self.div_count = 0 
        self.name = "" 
        self.tags = "" 
        self.ques_starttag_text = ""
        self.ans_starttag_text = ""
        self.params_starttag_text = ""
        self.mode = None 
        self.parts = {
            "params": "", 
            "javascript": "", 
            "library": "", 
            "question": "", 
            "answer": ""
        }

    def attrs_contains(self, attrs, name, value):
        for n, v in attrs:
          if n == name and v == value:
            return True
        return False

    def handle_starttag(self, tag, attrs):
        if tag == "div":
            self.div_count += 1; 
            if self.attrs_contains(attrs, "id", "question"):
                self.mode = "question"
                self.ques_starttag_text = self.get_starttag_text()
            elif self.attrs_contains(attrs, "id", "answer"):
                self.mode = "answer"
                self.ans_starttag_text = self.get_starttag_text()

        if tag == "script":
            if self.attrs_contains(attrs, "class", "library"): 
                self.mode = "library"
            elif self.attrs_contains(attrs, "id", "params"): 
                self.mode = "params"
                self.params_starttag_text = self.get_starttag_text()
            elif self.attrs_contains(attrs, "id", "javascript"): 
                self.mode = "javascript"

        if self.mode is not None:
            self.parts[self.mode] += self.get_starttag_text()

    def handle_startendtag(self, tag, attrs): 
        if self.mode is not None:
            self.parts[self.mode] += self.get_starttag_text()

    def handle_endtag(self, tag):

        if self.mode is not None:
            self.parts[self.mode] += "</" + tag + ">"

        if tag == "div":
            self.div_count -= 1; 

            if self.div_count == 0:
                self.mode = None 

        if tag == "script":
            self.mode = None 

    def handle_comment(self, data):
        if self.mode is not None:
            self.parts[self.mode] += "<!--" + data + "-->" 
        else:
            self.name = extract(data, '{% name "', '" %}') 
            self.tags = extract(data, '{% tags "', '" %}') 
        
    def handle_data(self, data):
        if self.mode is not None:
            self.parts[self.mode] += data 

    def get_name(self):
        return self.name

    def get_tags(self):
        return self.tags

    def get_question(self):
        q = self.parts["question"].lstrip(self.ques_starttag_text) 
        q = q.rstrip("</div>").strip()
        return q

    def get_answer(self):
        a = self.parts["answer"].lstrip(self.ans_starttag_text) 
        a = a.rstrip("</div>").strip()
        return a 

    def get_params(self):
        p = self.parts["params"].lstrip(self.params_starttag_text) 
        p = p.rstrip("</script>")
        params = extract(p,"=",";")
        return params 

    def get_javascript(self):
        return self.parts["javascript"]

    def get_library(self):
        return self.parts["library"]

    def result(self):
        return self.html 

def extract(text, sub1, sub2):
    """extract a substring between two substrings sub1 and sub2 in text"""
    return text.split(sub1)[-1].split(sub2)[0].strip()

class HtmlTest(unittest.TestCase):

    def setUp(self):
        '''Verify environment is setup properly''' 
 
    def tearDown(self):
        '''Verify environment is torn down properly''' 

    def test_params(self):
        '''Verify parser finds params''' 
        parser = MathplosionParser()
        parser.feed(html)
        print "-----params----"
        print parser.get_params()
        print "-----javascript----"
        print parser.get_javascript()
        print "-----library----"
        print parser.get_library()
        print "-----question----"
        print parser.get_question()
        print "-----answer----"
        print parser.get_answer()
        print "-----name----"
        print parser.get_name()
        print "-----tags----"
        print parser.get_tags()
        #print "-----html----"
        #print parser.result()

#    def test_parser(self):
#        '''Verify parser finds div''' 
#        parser = MathplosionParser()
#        parser.feed(html)
#        print parser.result()
#        self.assertEqual(html,parser.result())


if __name__=='__main__':
    unittest.main()
