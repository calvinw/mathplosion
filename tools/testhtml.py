#!/usr/bin/env python
 
import unittest 
import re
from BeautifulSoup import BeautifulSoup

thediv = """<div id="question"> Hi there <div> Blah </div> bye there </div>"""



pattern = '<div id=/"question/">.*?((?<TAG><div).*?(?<-TAG></div>))?(?(TAG)(?!))</div>'
match = re.match(pattern, thediv)
print match.group()

doc = ['<script id="params">var json={"x":{"type":"int"}}</script>',
      '<script id="javascript">update_params(params){return;}</script>',
      '<body>',
      '<div id="question"><p>This is the question</p>     <p>Hello there</p></div>',
      '<div id="answer"><p>This is the answer</p></div>',
      '</body>']
 
def read_html():
    '''A simple adding function to demo unittest'''
    soup = BeautifulSoup(''.join(doc))
    return soup
 
class ReadHtmlTest(unittest.TestCase):

    def setUp(self):
        '''Verify environment is setup properly''' 
 
    def tearDown(self):
        '''Verify environment is torn down properly''' 

    def test_find_params(self):
        '''Verify can find params''' 
        soup = read_html()
        params = soup.find(id="params")
        self.assertEqual(doc[0],str(params))
        self.assertEqual('var json={"x":{"type":"int"}}', params.renderContents())

    def test_find_javascript(self):
        '''Verify can find javascript''' 
        soup = read_html()
        javascript = soup.find(id="javascript")
        self.assertEqual(doc[1],str(javascript))
        self.assertEqual('update_params(params){return;}', javascript.renderContents())

    def test_find_question(self):
        '''Verify can find question''' 
        soup = read_html()
        question = soup.find(id="question")
        self.assertEqual(doc[3],str(question))
        #self.assertEqual('<p>This is the question</p> <p>Hello there</p>',question.renderContents())

    def test_find_answer(self):
        '''Verify can find answer''' 
        soup = read_html()
        answer = soup.find(id="answer")
        self.assertEqual(doc[4],str(answer))
        self.assertEqual('<p>This is the answer</p>', answer.renderContents())

if __name__=='__main__':
    unittest.main()
