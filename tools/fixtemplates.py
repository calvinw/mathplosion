#!/usr/bin/env python
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from editor.models import Problem, Template
from django.conf import settings

#problems = Problem.objects.all()

#for problem in problems:
#    js = problem.javascript

#    copy = js.replace(
#    'http://mathplosion.com',
#    'http://127.0.0.1:8000',
#    )

#    copy = js.replace('id="javascript>','id="javascript">')

#    problem.javascript = copy
#    problem.save()

#    print problem.id 
#    print js[0:200] 

templates = Template.objects.all()
for template in templates:
    js = template.javascript

    copy = js.replace('<script type="text/javascript">','<script id="javascript">')

    copy = copy.replace('<script type="text/javascript" src="/media/normal.js"></script>','<script class="library" src="http://mathplosion.com/media/normal.js"></script>')

    template.javascript = copy
    template.save()

    print template.id 
    print js[0:200]
