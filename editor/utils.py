from django.conf import settings
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from taggit.models import TaggedItem, Tag
from editor.models import MpTag, Template  
from editor.forms import TagsForm
import os
import HTMLParser

class MathplosionParser(HTMLParser.HTMLParser):

    def __init__(self):

        HTMLParser.HTMLParser.__init__(self)
        self.div_count = 0 
        self.name = "" 
        self.tags = "" 
        self.ques_starttag_text = ""
        self.ans_starttag_text = ""
        self.params_starttag_text = ""
        self.mode = None 
        self.parts = {
            "params": "", 
            "javascript": "", 
            "library": "", 
            "question": "", 
            "answer": ""
        }

    def attrs_contains(self, attrs, name, value):
        for n, v in attrs:
          if n == name and v == value:
            return True
        return False

    def handle_starttag(self, tag, attrs):
        if tag == "div":
            self.div_count += 1; 
            if self.attrs_contains(attrs, "id", "question"):
                self.mode = "question"
                self.ques_starttag_text = self.get_starttag_text()
            elif self.attrs_contains(attrs, "id", "answer"):
                self.mode = "answer"
                self.ans_starttag_text = self.get_starttag_text()

        if tag == "script":
            if self.attrs_contains(attrs, "class", "library"): 
                self.mode = "library"
            elif self.attrs_contains(attrs, "id", "params"): 
                self.mode = "params"
                self.params_starttag_text = self.get_starttag_text()
            elif self.attrs_contains(attrs, "id", "javascript"): 
                self.mode = "javascript"

        if self.mode is not None:
            self.parts[self.mode] += self.get_starttag_text()

    def handle_startendtag(self, tag, attrs): 
        if self.mode is not None:
            self.parts[self.mode] += self.get_starttag_text()

    def handle_endtag(self, tag):

        if self.mode is not None:
            self.parts[self.mode] += "</" + tag + ">"

        if tag == "div":
            self.div_count -= 1; 

            if self.div_count == 0:
                self.mode = None 

        if tag == "script":
            self.mode = None 

    def handle_comment(self, data):
        if self.mode is not None:
            self.parts[self.mode] += "<!--" + data + "-->" 
        
    def handle_data(self, data):
        if self.mode is not None:
            self.parts[self.mode] += data 

    def get_question(self):
        q = self.parts["question"].lstrip(self.ques_starttag_text) 
        q = q.rstrip("</div>").strip()
        return q

    def get_answer(self):
        a = self.parts["answer"].lstrip(self.ans_starttag_text) 
        a = a.rstrip("</div>").strip()
        return a 

    def get_params(self):
        p = self.parts["params"].lstrip(self.params_starttag_text) 
        p = p.rstrip("</script>")
        params = extract(p,"=",";")
        return params 

    def get_javascript(self):
        return self.parts["javascript"]

    def get_library(self):
        return self.parts["library"]

    def result(self):
        return self.html 

def build_template_html(template):

    """Build and html file for saving out the template fields"""
    tagslist = [tag.name for tag in template.tags.all()]
    tags_as_csv = ",".join(tagslist)

    name = template.name.strip()
    tags = tags_as_csv.strip()
    params = template.params.strip()
    javascript = template.javascript.strip()
    question = template.question_template.strip()
    answer = template.answer_template.strip()

    media_url = settings.MEDIA_URL
    jquery_url = media_url + "js/jquery-1.7.1.min.js" 

    s = '<script src="'+ jquery_url + '"></script>' + '\n' + \
        '<script src="'+ media_url + 'mathplosion.js"></script>' + '\n' + \
        '\n' + \
        '<!--name "' + name + '"-->' + '\n' + \
        '<!--tags "' + tags + '"-->' + '\n' + \
        '\n' + \
        '<script id="params">' + '\n' + \
        'var json_params=' + '\n' + \
        params + '\n' + \
        '</script>' + '\n' + \
        '\n' + \
        javascript + '\n' + \
        '<form id="form"></form>' + '\n' + \
        '\n' + \
        '<h2>Question:</h2>' + '\n' + \
        '<div id="question">' + '\n' + \
        question + '\n' + \
        '</div>' + '\n' + \
        '\n' + \
        '<h2>Answer:</h2>' + '\n' + \
        '<div id="answer">' + '\n' + \
        answer + '\n' + \
        '</div>' + '\n'

    s = s.replace("\r\n", "\n")
    return s

def extract_template_html(template, html):
    parser = MathplosionParser()
    parser.feed(html)

    template.params = parser.get_params()
    template.javascript = parser.get_library() + '\n' + parser.get_javascript()
    template.question_template = parser.get_question()
    template.answer_template = parser.get_answer()
    return template

def update_from_file(template):

    name = settings.ROOT_PATH + "/editor/fixtures/" + template.filename 

    f = open(name,"r")
    html = f.read() 

    template = extract_template_html(template, html)
    name = extract(html, '<!--name "','"-->')
    tags = extract(html, '<!--tags "','"-->')

    template.name=name
    template.question = ""
    template.answer = ""

    user = User.objects.get(username = "mathplosion")

    #remove white space 
    tags = ''.join(tags.split())

    taglist = tags.split(",")
    template.replace_tags(taglist)
    template.save()

def extract(text, sub1, sub2):
    """extract a substring between two substrings sub1 and sub2 in text"""
    return text.split(sub1)[-1].split(sub2)[0].strip()

def filter_tags2(queryset, tags):
    """Given tags separated by commas, filter the queryset with these tags"""
    taglist = tags.split(",")
    for tag in taglist:
        tag_as_list = [tag]
        queryset=queryset.filter(tags__name__in=tag_as_list)

    return queryset

def filter_tags(queryset, tags):
    """Given tags separated by /, filter the queryset with these tags"""
    taglist = get_taglist(tags)
    for tag in taglist:
        tag_as_list = [tag]
        queryset=queryset.filter(tags__name__in=tag_as_list)

    return queryset

def create_tagsform(tags):
    taglist = get_taglist(tags)
    if taglist:
        initial={'tags': ",".join(taglist)}
    else:
        initial = None
    return TagsForm(initial)

def get_taglist(tags):
    """Given tags separated by /, return a list of these tags"""

    if tags is not None:
        taglist = tags.split("/")
    else:
        taglist = [] 

    return taglist

def move(queryset, direction, orig_position, new_position):
    """Takes an ordered queryset and moves the entry at orig position to new position. Moves everything between the orig and new by one either up or down"""

    # change to 0-based index
    orig = orig_position - 1
    new = new_position - 1

    item = queryset[orig]   

    # must move items are between orig and the new one 
    if direction == "up": 
        must_move = queryset[new:orig] 
    elif direction == "down":
        must_move = queryset[orig+1:new+1] 

    # must move items move either up or down by 1 
    for i in must_move:
        if direction == "up": 
            i.position = i.position + 1
        elif direction == "down":
            i.position = i.position - 1

        i.save()

    item.position = new_position 
    item.save()

    return

def index(queryset, id):
    """Takes a queryset returns the 0-based index and model for it."""
    d = {}
    for i,item in enumerate(queryset):
        if item.id==int(id):
            d['index']=i 
            d['item']=item
            return d
