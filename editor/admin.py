from django.contrib import admin
from editor.models import Problem,Template,List,MpTag,UserProfile

admin.site.register(Problem)
admin.site.register(Template)
admin.site.register(UserProfile)
admin.site.register(List)
admin.site.register(MpTag)
