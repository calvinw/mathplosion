from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.context_processors import csrf
from django.db.models import Count
from django.template import RequestContext 
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.safestring import SafeUnicode
from taggit.managers import TaggableManager
from taggit.models import TaggedItem, Tag
from models import *
from mathplosion.views import HttpResponseReload  
from forms import *
from utils import MathplosionParser,extract,create_tagsform,get_taglist,filter_tags,filter_tags2,move,index,extract_template_html,build_template_html
from forms import TemplateForm,ProblemForm
import re
from copy import deepcopy
import os
import glob
import urllib2

def form_errors(request, form):
    """Return the errors from a form"""
    context = RequestContext(request)
    context['form'] = form
    context.update(csrf(request))
    return render_to_response("formerrors.html", context)

@login_required
def change_list_settings(request, id):
    """Put up page that lets user change name and tags of the problem set."""
    list = get_object_or_404(List, id=id)
    context = RequestContext(request)
    listform = ListForm(instance=list)
    context['list'] = list
    context['listform'] = listform 
    context['main_html'] =  'change_list_settings_main.html'
    return render_to_response('editor_base.html', context)

@login_required
def change_template_settings(request, tid):
    """Put up page that lets user change name, tags, filename url"""
    template = get_object_or_404(Template, id=tid)
    templatesettingsform = TemplateSettingsForm(instance=template)

    context = RequestContext(request)
    context['tid'] = tid 
    context['templatesettingsform'] = templatesettingsform
    context['template'] = template
    context['main_html'] =  'change_template_settings_main.html'

    context.update(csrf(request))

    return render_to_response("editor_base.html", context)


@login_required
def save_list_settings(request, id):
    """Saves the changes to the list settings. Go back to the list after."""
    if request.method == 'POST':
        list = get_object_or_404(List, id=id)
        listform = ListForm(request.POST, instance=list)

        if not listform.is_valid():
            return form_errors(request, listform)

        name = listform.cleaned_data['name']
        list.name = name

        tags = listform.cleaned_data['tags']
        list.replace_tags(tags)
        list.save()

        url = reverse("editor.views.lists")
        return HttpResponseRedirect(url)
    return HttpResponseServerError(u"No POST data sent.")

@login_required
def new_list(request):
    """Put up page to let the user create a new list."""
    context = RequestContext(request)
    user = User.objects.get(username = request.user)
    listform = ListForm()
    context['listform'] = listform 
    context['main_html'] =  'newlist_main.html'
    return render_to_response('editor_base.html', context)

@login_required
def new_template(request):
    """Create a template and start editing it."""
    user = User.objects.get(username = request.user)
    template = Template.objects.create(user=user)
    url = reverse('editor.views.change_template_settings', args=[template.id])
    return HttpResponseRedirect(url)

@login_required
def ajax_lists(request):
    """List of problem sets, filtered by tags."""
    user = User.objects.get(username = request.user)
    lists = user.list_set.all()

    tags = request.GET.get("tags", None)
    if tags is not None:
        lists = filter_tags2(lists, tags)

    context = RequestContext(request)
    context['lists'] = lists
    context.update(csrf(request))
    return render_to_response('ajax_lists.html', context)

@login_required
def lists(request, tags=None):
    """The UI for the problem sets for a user."""

    user = User.objects.get(username = request.user)

    lists = user.list_set.all()
    lists = filter_tags(lists, tags)

    tagsform = create_tagsform(tags)

    context = RequestContext(request)
    context['lists'] = lists
    context['tagsform'] = tagsform
    context['main_html'] = 'lists_main.html' 
    context.update(csrf(request))

    return render_to_response('editor_base.html', context)

def get_session_templates_mode(request):
   if 'templates_mode' not in request.session:
      request.session['templates_mode'] = 'mathplosion'
   return request.session['templates_mode'] 

def set_session_templates_mode(request, mode):
   request.session['templates_mode'] = mode

def get_session_edit_mode(request):
   if 'edit_mode' not in request.session:
      request.session['edit_mode'] = 'edit'
   return request.session['edit_mode'] 

def set_session_edit_mode(request, mode):
   request.session['edit_mode'] = mode

def filter_templates(mode, user):
    if mode == "mathplosion":
        mp_user = User.objects.get(username = "mathplosion")
        templates = mp_user.template_set.all()
    elif mode == "user":
        templates = user.template_set.all()
    elif mode == "other":
        mp_user = User.objects.get(username = "mathplosion")
        user = User.objects.get(username = user.username)
        templates = Template.objects.exclude(user=mp_user).exclude(user=user)
    else:
        raise ValueError 

    return templates

def templates_by_category(mode, user, category):
    """List of geometry templates for a user, Mode is 'user','mathplosion', or 'other'"""
    templates = filter_templates(mode, user)
    templates_category = filter_tags2(templates, category)
    return templates_category

def ajax_templates(request, listid=None, mode=None):
    """List of templates for a user, filtered by tags. Mode is 'user' or 'mathplosion'"""
    if mode is not None:
        set_session_templates_mode(request,mode)
    else:
        mode = get_session_templates_mode(request)

    #print "in ajax_templates: " + request.session["templates_mode"]

    try: 
        templates = filter_templates(mode, request.user)
    except: 
        return HttpResponseServerError("Cannot find template mode")

    #print "here "
    if listid is not None:
        #print "listid is " + listid
        list = get_object_or_404(List, id=listid)
    else:
        #print "listid was None"
        list = None 

    #tags = request.GET.get("tags", None)
    #if tags is not None:
    #    templates = filter_tags2(templates, tags)

    context = RequestContext(request)

    context['templates'] = templates

#    context['statistics_templates'] = templates_by_category(mode, request.user, "statistics")
#    context['algebra_templates'] = templates_by_category(mode, request.user, "algebra")
#    context['geometry_templates'] = templates_by_category(mode, request.user, "geometry")
#    context['demo_templates'] = templates_by_category(mode, request.user, "demo")
#    context['basic_templates'] = templates_by_category(mode, request.user, "basic")
    context['listid'] = listid
    context['list'] = list

    return render_to_response('ajax_templates.html', context) 
    #return render_to_response('templates_categories.html', context) 

@login_required
def tags(request, mode="mathplosiontemplates"):
    """UI for showing tags. Mode is the type of tags, templates or problemsets"""
    context = RequestContext(request)

    if mode == "templates": 
        model = "template"
        user = User.objects.get(username = request.user)
    elif mode == "lists": 
        model = "list"
        user = User.objects.get(username = request.user)
    elif mode == "mathplosiontemplates": 
        model = "template"
        user = User.objects.get(username = "mathplosion")

    # This annotates each tag with the number of TaggedItems there are of a particular model (Template, List) and with user given by the current user. So you will not see other peoples tags (on Lists or non-mathplosion Templates) 
    qs = MpTag.objects.filter(editor_mptaggeditem_items__content_type__model=model, editor_mptaggeditem_items__user=user).annotate(num_times=Count('editor_mptaggeditem_items'))
    qs = qs.order_by('-num_times')        

    context['tags'] = qs
    context['mode'] = mode

    return render_to_response('tags.html', context)

@login_required
def templates(request, id=None, tags=None, mode=None):
    """UI for showing templates. If an id is passed it is the list id to add problems to. If a mode is passed it tells what users templates to display""" 

    if mode is not None:
        set_session_templates_mode(request,mode)
    else:
        mode = get_session_templates_mode(request)

    #print "in templates: " + request.session["templates_mode"]

    try: 
        templates = filter_templates(mode, request.user)
    except: 
        return HttpResponseServerError("Cannot find template mode")

    if id is not None:
        list = get_object_or_404(List, id=id)
        listname = list.name
        listid = id
    else:
        list = None
        listname = None
        listid = None 

    user = User.objects.get(username = request.user)
    lists = user.list_set.all()
    tagsform = create_tagsform(tags)

    context = RequestContext(request)
    context['list'] = list
    context['listid'] = listid 
    context['listname'] = listname

    context['lists'] = lists
    context['tagsform'] = tagsform
    context['templates'] = templates
#    context['statistics_templates'] = templates_by_category(mode, request.user, "statistics")
#    context['algebra_templates'] = templates_by_category(mode, request.user, "algebra")
#    context['geometry_templates'] = templates_by_category(mode, request.user, "geometry")
#    context['demo_templates'] = templates_by_category(mode, request.user, "demo")
#    context['basic_templates'] = templates_by_category(mode, request.user, "basic")
    context['main_html'] = 'templates_main.html' 

    return render_to_response('editor_base.html', context)

@login_required
def view_list(request, id):
    """View all problems in a Problem Set.Used before Publishing"""
    list = get_object_or_404(List, id=id)
    problems = list.problems()
    context = RequestContext(request)
    context['problems'] = problems
    context['list'] = list
    context['main_html'] = 'share_it_main.html' 
    return render_to_response('editor_base.html', context)

def load_template_from_jsbin(filename):
    html = ""
    jsbinurl = filename + "/latest/download"
    #print jsbinurl
    try: 
        response = urllib2.urlopen(jsbinurl)
        html = response.read()
    except: 
        pass 
    return html

def load_template_from_local_file(filename):
    html = ""
    name = settings.ROOT_PATH + "/examples/templates/" + filename 
    try: 
        f = open(name,"r")
        html = f.read() 
    except: 
        pass 
    return html


@login_required
def import_template(request, tid):
    """UI for showing the import template page. This puts up a page where users can preview and then import the template from jsbin or local file system.""" 
    user = User.objects.get(username = request.user)
    template = get_object_or_404(Template, id=tid)

    #print "in import_template"
    #print "template.filename is: " + template.filename 

    if template.filename.startswith("http://jsbin.com"):
        html = load_template_from_jsbin(template.filename)
    else:
        html = load_template_from_local_file(template.filename)

    if html != '': 
        parser = MathplosionParser()
        parser.feed(html)

        template.params = parser.get_params();
        template.javascript = parser.get_library() + '\n' + parser.get_javascript()
        template.question_template = parser.get_question()
        template.answer_template = parser.get_answer()

        templateform = TemplateForm(instance=template)

        context = RequestContext(request)

        context['tid'] = tid 
        context['templateform'] = templateform
        context['template'] = template
        context['main_html'] = 'import_template_main.html'

        context.update(csrf(request))

        return render_to_response('editor_base.html', context)
    else:
        return HttpResponseServerError("Can't find file for template.")

@login_required
def output(request, id):
    list = get_object_or_404(List, id=id)
    problems = list.problems()

    context = RequestContext(request)
    context['theme'] = request.GET.get('theme', 'yes') 
    context['show'] = request.GET.get('show', 'questions') 
    context['style'] = request.GET.get('style', 'numbers') 
    context['name'] = request.GET.get('name', list.name) 
    context['problems'] = problems

    return render_to_response('output.html', context)

def single_page_output(request, id):
    """This shows the single page output"""
    list = get_object_or_404(List, id=id)
    problems = list.problems()

    context = RequestContext(request)
    context['problems'] = problems
    context['previewtype'] = request.GET.get('previewtype', 'both') 
    context['numbering'] = request.GET.get('numbering', 'yes') 
    context['title'] = request.GET.get('title') 

    return render_to_response('output.html', context)

@login_required
def create_problems(request):
    """Creates all the problems once we know which templates were chosen"""
    error_msg = u"No POST data sent."
    if request.method == 'POST':
        post = request.POST.copy()
        if post.has_key('listid'):
            listid = post['listid'] 
            #print "the listid is" + listid
            if listid =='0':
                return HttpResponseServerError("You must choose a problem set")
            list = get_object_or_404(List, id=listid)
            user = User.objects.get(username = request.user)
            num_problems = list.problems().count()
            position = num_problems

            for key in post:
                if key.startswith("template"):
                    #print "found a problem to add" + key

                    template = get_object_or_404(Template, id=post[key])

                    if template.question:
                        question = template.question
                    else:  
                        question = template.question_template

                    if template.answer:
                        answer = template.answer
                    else:  
                        answer = template.answer_template

                    position = position + 1
                    problem = Problem.objects.create(list=list, name=template.name, 
                        question=question, answer=answer, javascript=template.javascript, position=position, 
                        user=user, template=template, params=template.params)

                    tags_list = template.tags.all()
                    problem.replace_tags(tags_list)
                    problem.save()

            url = reverse('editor.views.list', args=[list.id])
            return HttpResponseRedirect(url)
        else:
            error_msg = u"No list id sent."

    return HttpResponseServerError(error_msg)

@login_required
def ajax_move_list(request, id=None, direction="up"):

    tags = request.GET.get("tags", None)

    user = User.objects.get(username = request.user)
    lists = user.list_set.all()

    if tags is not None:
        filtered_lists = filter_tags2(lists, tags)
    else:
        filtered_lists = lists
    
    dict = index(filtered_lists, id)
    i = dict['index']
    list = dict['item']

    if direction=="up":
        if i > 0:
            list_above = filtered_lists[i-1] 
            move(lists, "up", list.position, list_above.position)    
    elif direction=="down":
        l = len(filtered_lists) 
        if i < l-1:
            list_below = filtered_lists[i+1] 
            move(lists, "down", list.position, list_below.position)    
    else:
        return HttpResponseServerError("Can't move list: direction unknown")

    return ajax_lists(request)

@login_required
def generate_one(request, tid):
    if request.method == 'POST':
        template = get_object_or_404(Template, id=tid)

        problemform = ProblemForm(request.POST, instance=template)
        if not problemform.is_valid(): 
            return form_errors(request,problemform)

        template = problemform.save(commit=False)

        context = RequestContext(request)
        context['problem'] = template 
        context.update(csrf(request))
        return render_to_response("problem.html", context)
    else: 
        return HttpResponseServerError(u"No POST data sent.")

@login_required
def get_one(request, tid):

    template = get_object_or_404(Template, id=tid)
    problemform = ProblemForm(instance=template)

    context = RequestContext(request)
    context['problemform'] = problemform 
    context['problem'] = template 
    context['name'] = template.name
    context['tags'] = template.tags
    context['main_html'] = 'edit_problem_main.html'
    context.update(csrf(request))

    return render_to_response('editor_base.html', context)

login_required
def ajax_move_problem(request, pid=None, direction="up"):

    p = get_object_or_404(Problem, id=pid)
    num_problems = p.list.problems().count()
    do_save = True

    if direction == "up" and p.position > 1:
        other_p = p.list.problem_set.get(position = p.position-1)
    elif direction == "down" and p.position < num_problems:
        other_p = p.list.problem_set.get(position = p.position+1)
    else:
        do_save = False 

    if do_save is True:
        p.position, other_p.position = other_p.position, p.position
        p.save()
        other_p.save()

    return ajax_list(request,id=p.list.id)

@login_required
def create_list(request):
    """Creates a problem set."""
    error_msg = u"No POST data sent."
    if request.method == "POST":

        listform = ListForm(request.POST)

        if not listform.is_valid(): 
            return form_errors(request,listform)

        tags = listform.cleaned_data['tags']
        name = listform.cleaned_data['name']

        user = User.objects.get(username = request.user)
        count = user.list_set.all().count()

        list = List.objects.create(name=name, user=user, position=count+1)
        list.save()

        list.replace_tags(tags)

        url = reverse('editor.views.list', args=[list.id])
        return HttpResponseRedirect(url)
    return HttpResponseServerError(error_msg)

@login_required
def save_template_settings(request, tid):
    if request.method == 'POST':
        post = request.POST.copy()
        user = User.objects.get(username = request.user)
        template = get_object_or_404(Template, id=tid)
    
        templatesettingsform = TemplateSettingsForm(request.POST, instance=template)
        if not templatesettingsform.is_valid():
            return form_errors(request, templatesettingsform)

        # fills is template.name, template.filename
        template=templatesettingsform.save(commit=False)

        # fills is template tags 
        tags = templatesettingsform.cleaned_data['tags'] 
        template.replace_tags(tags)
        template.save()
        url = reverse('editor.views.templates')
        return HttpResponseRedirect(url)
    else:
        return HttpResponseServerError(u"No POST data sent.")

@login_required
def save_import_template(request, tid=None):
    """This actually imports the jsbin or local file template after the user selects import to Mathplosion."""
    if request.method == 'POST':
        post = request.POST.copy()
        user = User.objects.get(username = request.user)

        if post.has_key("import"):
            template = get_object_or_404(Template, id=tid)

            templateform = TemplateForm(request.POST, instance=template)
            if not templateform.is_valid():
                return form_errors(request, templateform)

            # fills in template.name, template.question, template.answer 
            template=templateform.save(commit=False)

            # fills in template tags 
            tags = templateform.cleaned_data['tags'] 
            template.replace_tags(tags)

            if template.filename.startswith("http://jsbin.com"):
                html = load_template_from_jsbin(template.filename)
            else:
                html = load_template_from_local_file(template.filename)

            if html != "":
                # fills in template question template, answer template 
                template = extract_template_html(template, html) 
                template.save()
            else: 
                return HttpResponseServerError(u"Could not load html for template.")

            url = reverse('editor.views.templates')
            return HttpResponseRedirect(url)
        else:
            return HttpResponseServerError(u"POST did not contain import.")
    else:
        return HttpResponseServerError(u"No POST data sent.")

@login_required
def save_template(request, tid):
    """Save a template after editing."""
    if request.method == 'POST':
        post = request.POST.copy()
        user = User.objects.get(username = request.user)

        template = get_object_or_404(Template, id=tid)

        templateform = TemplateForm(request.POST, instance=template)
        if not templateform.is_valid():
            return form_errors(request, templateform)

        template=templateform.save(commit=False)
        tags = templateform.cleaned_data['tags'] 
        template.replace_tags(tags)

        codemirror_code = post['code'] 

        template = extract_template_html(template, codemirror_code) 
        template.save()

        url = reverse('editor.views.templates')
        return HttpResponseRedirect(url)
    else:
        return HttpResponseServerError(u"No POST data sent.")

@login_required
def save(request, pid):
    """Save a problem after editing it."""
    if request.method == 'POST':
        problem = get_object_or_404(Problem, id=pid)

        problemform = ProblemForm(request.POST, instance=problem)
        if not problemform.is_valid(): 
            return form_errors(request,problemform)

        problem = problemform.save()

        #url = reverse('editor.views.list', args=[problem.list.id])
        #return HttpResponseRedirect(url)
        return  HttpResponseReload(request)
    else:
        return HttpResponseServerError(u"No POST data sent.")

@login_required
def ajax_list(request, id=None, mode="edit"):
    """ List of problems. View in a mode and filtered by certain tags """    

    list = get_object_or_404(List, id=id)
    problems = list.problems()

    tags = request.GET.get("tags", None)
    if tags is not None:
        problems = filter_tags2(problems, tags)

    context = RequestContext(request)

    context['problems'] = problems
    context['list'] = list
    context['mode'] = mode

    return render_to_response("ajax_list.html", context)

@login_required
def list(request, id=None, tags=None):
    """UI to edit a problem set"""
    list = get_object_or_404(List, id=id)
    problems = list.problems()

    exam_tags = list.tags.all().order_by('name')
    exam_tags_as_list_of_strings = [tag.name for tag in exam_tags] 
    exam_tags_string = "/".join(exam_tags_as_list_of_strings)

    tagsform = create_tagsform(tags)

    context = RequestContext(request)

    context['list'] = list
    context['mode'] = "edit" 
    context['problems'] = problems 
    context['tags'] = exam_tags
    context['tags_string'] = exam_tags_string
    context['tagsform'] = tagsform 
    context['main_html'] = 'list_main.html' 

    context.update(csrf(request))

    return render_to_response('editor_base.html', context)


@login_required
def ajax_copy_problem(request, pid=None):
    """Make a copy of a problem"""
    user = User.objects.get(username = request.user)
    p = get_object_or_404(Problem, id=pid)
    copy = deepcopy(p)
    copy.position = p.position + 1
    copy.id = None
    
    must_move = p.list.problems()[p.position:]

    for prob in must_move:
        prob.position = prob.position+1
        prob.save()

    #need to save it to set the tags
    copy.save()
    tags_list = p.template.tags.all()
    copy.replace_tags(tags_list)
    copy.save()

    return ajax_list(request, id=p.list.id) 

@login_required
def ajax_copy_template(request, tid=None):
    """Make a copy of a template"""

    user = User.objects.get(username = request.user)
    template = get_object_or_404(Template, id=tid)

    copy = Template.objects.create(user=user)
    copy.name = "Copy of " + template.name
    copy.params = template.params
    copy.javascript = template.javascript
    copy.question_template = template.question_template
    copy.answer_template = template.answer_template
    copy.question = template.question
    copy.answer = template.answer

    # Copy the tags too.
    tags_list = template.tags.all()
    copy.replace_tags(tags_list)

    copy.save()
        
    return ajax_templates(request)

@login_required
def view_problem(request, pid):
    """View a problem"""
    user = User.objects.get(username = request.user)

    problem = get_object_or_404(Problem, id=pid)
    problemform = ProblemForm(instance=problem)

    context = RequestContext(request)
    context['list'] = problem.list 
    context['problem'] = problem
    context['name'] = problem.name
    context['tags'] = problem.tags
    context['main_html'] = 'problem_main.html'

    context.update(csrf(request))

    return render_to_response('editor_base.html', context)

@login_required
def edit_problem(request, pid):
    """Edit a problem"""
    user = User.objects.get(username = request.user)

    problem = get_object_or_404(Problem, id=pid)
    problemform = ProblemForm(instance=problem)

    context = RequestContext(request)
    context['problemform'] = problemform 
    context['list'] = problem.list 
    context['problem'] = problem
    context['name'] = problem.name
    context['tags'] = problem.tags
    context['main_html'] = 'edit_problem_main.html'

    context.update(csrf(request))

    return render_to_response('editor_base.html', context)

@login_required
def delete_problems(request, id):
    """Delete all problems on a problem set"""
    list = get_object_or_404(List, id=id)
    problems = list.problems()
    problems.delete()
    url = reverse('editor.views.list', args=[id])
    return HttpResponseRedirect(url)

@login_required
def ajax_delete_template(request, tid=None):
    """Delete a template. This currently deletes all problems having this template"""
    template = get_object_or_404(Template, id=tid)
    template.delete()
    return ajax_templates(request)

@login_required
def ajax_delete_list(request, id=None):
    """Delete a problem set"""

    user = User.objects.get(username = request.user)
    list = get_object_or_404(List, id=id)
    lists = user.list_set.all()
    tail = lists[list.position:]

    for l in tail:
        l.position = l.position-1
        l.save()

    list.delete()

    return ajax_lists(request)

@login_required
def ajax_delete_problem(request, pid=None):
    """Delete a single problem"""
    user = User.objects.get(username = request.user)
    problem = get_object_or_404(Problem, id=pid)
    tail = problem.list.problems()[problem.position:]

    # for each problem after this one... decrease the position column
    for p in tail:
        p.position = p.position-1
        p.save()

    problem.delete()

    return ajax_list(request, id=problem.list.id) 

@login_required
def selenium_save(request):

    user = User.objects.get(username = request.user)
    templates = user.template_set.all()

    context = RequestContext(request)
    context['templates'] = templates
    context.update(csrf(request))

    return render_to_response('selenium_save.html', context)

