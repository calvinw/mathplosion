from django.db import models
from models import *
from django import forms
from django.utils import simplejson as json
from taggit.forms import *

class UploadForm(forms.Form):
    file = forms.FileField()

class ProblemForm(forms.ModelForm):
    class Meta:
        model = AbstractProblem
        fields = ('params',
                  'question',
                  'answer')
        widgets = {
            'params': forms.HiddenInput,
            'question': forms.HiddenInput,
            'answer': forms.HiddenInput,
        }

class TemplateForm(forms.ModelForm):
    class Meta:
        model = Template 
        fields = ('name',
                  'tags',
#                  'filename',
                  'question',
                  'answer') 
        widgets = {
            'question': forms.HiddenInput,
            'answer': forms.HiddenInput,
        }

class TemplateSettingsForm(forms.ModelForm):
    class Meta:
        model = Template 
        fields = ('name',
                  'tags',
                  'filename')

class TagsForm(forms.Form):
    tags = TagField(required=False)

class ListForm(forms.ModelForm):
    class Meta:
        model = List 
        fields = ('name', 'tags') 
