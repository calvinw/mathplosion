from django import template

register = template.Library()

def bootstrap_base():
    #base_path = "//netdna.bootstrapcdn.com" 
    base_path = "/media"
    return base_path 

def bootstrap_version():
    return "2.2.2" 

def bootstrap_css_path(context):

    request = context['request']

    if 'theme' not in request.session:
        request.session['theme'] = 'bootstrap'

    theme = request.session['theme']

    if theme == 'bootstrap':
        path = bootstrap_base() + "/twitter-bootstrap/2.2.2/css/bootstrap.no-icons.min.css"
    else:
        path = bootstrap_base() + "/bootswatch/2.2.2/" + request.session['theme'] + "/bootstrap.no-icons.min.css"

    return path

register.simple_tag(takes_context=True)(bootstrap_css_path)

def bootstrap_responsive_css_path(context):
    request = context['request']
    path = bootstrap_base() + "/twitter-bootstrap/2.2.2/css/bootstrap-responsive.min.css"
    return path

register.simple_tag(takes_context=True)(bootstrap_responsive_css_path)

def fontawesome_css_path(context):
    request = context['request']
    path = bootstrap_base() + "/font-awesome/3.0/css/font-awesome.css"
    return path

register.simple_tag(takes_context=True)(fontawesome_css_path)

def bootstrap_js_path(context):
    path = bootstrap_base() + "/twitter-bootstrap/2.2.2/js/bootstrap.min.js"
    return path

register.simple_tag(takes_context=True)(bootstrap_js_path)

def icon(context, name):

    request = context['request']

    if 'theme' not in request.session:
       request.session['theme'] = 'bootstrap'

    theme = request.session['theme']

    list = ["slate", "cyborg", "spruce"]

    if theme in list: 
        classes_str = name + " " + "icon-white"
    else:
        classes_str = name

    return '<i class="' + classes_str + '"></i>'

register.simple_tag(takes_context=True)(icon)

def background_color_css(context):
    request = context['request']

    if 'theme' not in request.session:
       request.session['theme'] = 'bootstrap'

    theme = request.session['theme']

    list = ["slate", "cyborg", "spruce", "superhero"]

    if theme in list: 
        style_str =""
    else:
        style_str ="<style> body { background-color: #e8e8e8; } </style> "
    return style_str 

register.simple_tag(takes_context=True)(background_color_css)
