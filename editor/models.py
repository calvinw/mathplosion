from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from taggit.models import (TaggedItemBase, GenericTaggedItemBase, TaggedItem,
    TagBase, Tag)
from django.db.models.signals import post_save

class MpTag(TagBase):
    user = models.ForeignKey(User, editable=False, null=True)
    description = models.TextField()

class MpTaggedItem(GenericTaggedItemBase):
    user = models.ForeignKey(User, editable=False, null=True)
    tag = models.ForeignKey(MpTag, related_name="%(app_label)s_%(class)s_items")

    def __init__(self, *args, **kwargs):
        super(MpTaggedItem, self).__init__(*args, **kwargs)
        self.user = self.content_object.user

class AbstractBase(models.Model): 
    user = models.ForeignKey(User, editable=True)
    name = models.CharField(blank=True,max_length=100, null=True)
    filename = models.CharField(blank=True,max_length=100, null=True)
    position = models.IntegerField(editable=False, blank=True, null=True)
    tags = TaggableManager(through=MpTaggedItem, blank=True) 

    def tags_ordered_by_name(self):
        return self.tags.all().order_by('name')

    def add_tag(self, tag_string):
        tag, created = MpTag.objects.get_or_create(name=tag_string)
        if created:
            tag.user = self.user
            tag.save()
        self.tags.add(tag) 
        return       

    def replace_tags(self, taglist):
        self.tags.clear() 
        for tag in taglist:
            self.add_tag(tag)
        return       

    def replace_tags_csv(self, tags_csv):
        #remove white space
        tags_csv = ''.join(tags_csv.split())

        #make it a list 
        taglist = tags_csv.split(",")
        self.replace_tags(taglist)
        return       

    class Meta:
        ordering = ['position']
        abstract = True 

class List(AbstractBase):

    def problems(self):
        return self.problem_set.all().order_by('position')

    def __unicode__(self):
        return u"List %s" % (self.name)

class AbstractProblem(AbstractBase):
    params = models.TextField(blank=True,null=True) 
    question = models.TextField(blank=True,null=True)
    answer = models.TextField(blank=True,null=True)
    javascript = models.TextField(blank=True,null=True)

    class Meta:
        abstract = True 

class Template(AbstractProblem):
    question_template = models.TextField(blank=True,null=True)
    answer_template = models.TextField(blank=True,null=True)

    def tags_ordered_by_name(self):
        return self.tags.all().order_by('name')

    def __unicode__(self):
        return u"Template %s id: %s " % (self.name,self.id)

class Problem(AbstractProblem):
    list = models.ForeignKey(List, editable=False)
    template = models.ForeignKey(Template, blank=True, editable=False)

    def get_problem_type(self):
        return self.template.name

    def __unicode__(self):
        return u"Problem %s" % (self.id)

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        user = instance
        try: 
            user.get_profile()
        except UserProfile.DoesNotExist:
            if not user.is_staff:
                print "Creating a profile for the user: ", user.username
                UserProfile.objects.create(user=user)

post_save.connect(create_user_profile, sender=User)

class UserProfile(models.Model):
    user = models.OneToOneField(User)
