echo "removing mathplosiondev database"
rm mathplosiondev.sqlite
echo "syncing the database"
./manage.py syncdb --noinput
echo "running the server"
./manage.py runserver
