from django.conf.urls.defaults import *
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Example:
    # I dont know what this does?? but if I take it out... it doesnt work
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT }),
#   (r'^$', 'django.contrib.auth.views.login'),
    (r'^$', 'views.landing'),
    (r'^accounts/', include('registration.urls')),
    (r'^support/', 'views.support'),
    url(r'^theme/(?P<name>\w+)/', 'views.theme', name='theme'),
    (r'^tinymce/', 'views.tinymce'),
    (r'^tos/', 'views.tos'),
    (r'^mathplosion-dev/', 'views.dev'),
    (r'^mathplosion-users/', 'views.users'),
    (r'^videos/demo/', 'views.demovideo'),
    (r'^videos/tags/', 'views.tagsvideo'),
    (r'^videos/helpvideos/', 'views.helpvideos'),
    (r'^accounts/', include('registration.urls')),
    (r'^accounts/profile/$', 'views.loggedin'),
    (r'^accounts/logout', 'views.logout_view'),
    (r'^500/$', 'django.views.generic.simple.direct_to_template', {'template': '500.html'}),
    (r'^404/$', 'django.views.generic.simple.direct_to_template', {'template': '404.html'}),
    (r'^editor/', include('editor.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
)
