from django.template import RequestContext 
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.utils.encoding import iri_to_uri
from django.conf import settings

class HttpResponseReload(HttpResponse):
    """
    Reload page and stay on the same page from where request was made.
    """
    status_code = 302

    def __init__(self, request):
        HttpResponse.__init__(self)
        referer = request.META.get('HTTP_REFERER')
        self['Location'] = iri_to_uri(referer or "/")

def theme(request, name):
   context = RequestContext(request)
   request.session["theme"] = name 
   referer = request.META.get('HTTP_REFERER')
   return HttpResponseReload(request)

def tinymce(request):
    context = RequestContext(request)
    return render_to_response("index.html", context)

def tos(request):
    context = RequestContext(request)
    return render_to_response("tos.html", context)

def dev(request):
    context = RequestContext(request)
    return render_to_response("mathplosion-dev.html", context)

def users(request):
    context = RequestContext(request)
    return render_to_response("mathplosion-users.html", context)

def demovideo(request):
    context = RequestContext(request)
    return render_to_response("demovideo.html", context)

def tagsvideo(request):
    context = RequestContext(request)
    return render_to_response("tagsvideo.html", context)

def helpvideos(request):
    context = RequestContext(request)
    return render_to_response("helpvideos.html", context)

def tags(request):
    context = RequestContext(request)
    return render_to_response("demo.html", context)

def landing(request):
    context = RequestContext(request)
    return render_to_response("index.html", context)

def support(request):
    return redirect("http://support.mathplosion.com/") 
	
def loggedin(request):
    # Send it right on the Lists page
    url = reverse('editor.views.lists')
    return HttpResponseRedirect(url) 

def logout_view(request):
    if "theme" in request.session:
       theme = request.session["theme"]
    else:
       theme = "bootstrap"
        
    logout(request)

    request.session["theme"] = theme 
    url = reverse('views.landing')
    return HttpResponseRedirect(url) 
