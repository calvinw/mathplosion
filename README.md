##Mathplosion is an application for generating mathematics content##

It uses the [django web framework](http://www.djangoproject.com)

Please see the [Mathplosion wiki](https://bitbucket.org/calvinw/mathplosion/wiki/Home) for installation and setup instructions.

To use mathplosion, please visit [http://mathplosion.com](http://mathplosion.com)

There are two mailing lists related to mathplosion:

*     [mathplosion-users](https://groups.google.com/forum/?fromgroups#!forum/mathplosion-users) 
for questions about using mathplosion

*     [mathplosion-dev](https://groups.google.com/forum/?fromgroups#!forum/mathplosion-users) 
for questions about developing for mathplosion
