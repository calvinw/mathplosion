# Django settings for mathplosion project.
import os
ROOT_PATH = os.path.dirname(__file__)

ADMINS = (
)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Make this unique, and don't share it with anybody.
SECRET_KEY = '8lhg-j%$n$fpbn7ar5ujucz1=874n64vj7)%*q=is=bgp+oa#-'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(ROOT_PATH, 'templates'),
    os.path.join(ROOT_PATH, 'editor/templates'),
    os.path.join(ROOT_PATH, 'editor/fixtures'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.webdesign',
    'registration',
    'profiles',
    'editor',
)

SESSION_EXPIRE_AT_BROWSER_CLOSE = False 

try:
    # For production, settings will be settings_local. 
    from settings_local import *
except ImportError:
    # no production settings, so assume dev environment 
    import os
    ROOT_PATH = os.path.dirname(__file__)
    MEDIA_ROOT = os.path.join(ROOT_PATH, 'media')
    MEDIA_URL = 'http://127.0.0.1:8000/media/'
    ADMIN_MEDIA_PREFIX = '/admin_media/'

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(ROOT_PATH, 'mathplosion.sqlite'), #production database name 
            'USER': '',                      
            'PASSWORD': '',                 
            'HOST': '',                      
            'PORT': '',                  
        }
    }

    DEBUG = False 
    TEMPLATE_DEBUG = DEBUG

    #Debug toolbar
    DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}

    INTERNAL_IPS = ('127.0.0.1',)

    INTERCEPT_REDIRECTS = False

    ADDITIONAL_APPS = (
#        'debug_toolbar',
    )

    ADDITIONAL_MIDDLEWARE = (
#       'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

    DEBUG_TOOLBAR_PANELS = (
        'debug_toolbar.panels.version.VersionDebugPanel',
        'debug_toolbar.panels.timer.TimerDebugPanel',
        'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
        'debug_toolbar.panels.headers.HeaderDebugPanel',
        'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
        'debug_toolbar.panels.template.TemplateDebugPanel',
        'debug_toolbar.panels.sql.SQLDebugPanel',
    )

    # You can test emailing in the dev environment by using gmail:
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = 'user@gmail.com'
    EMAIL_HOST_PASSWORD = 'user gmail password'
    EMAIL_PORT = 587
    DEFAULT_FROM_EMAIL='user@gmail.com'

try:
    INSTALLED_APPS += ADDITIONAL_APPS
except:
    raise

try:
    MIDDLEWARE_CLASSES += ADDITIONAL_MIDDLEWARE
except:
    raise

ACCOUNT_ACTIVATION_DAYS = 2

SOUTH_MIGRATION_MODULES = {
#    'taggit': 'mathplosion.migrations.taggit',
#    'registration': 'mathplosion.migrations.registration',
#    'editor': 'mathplosion.migrations.editor',
}

AUTH_PROFILE_MODULE = "editor.UserProfile"

ALLOWED_HOSTS = ['mathplosion.com', '143.244.168.213', 'localhost']
